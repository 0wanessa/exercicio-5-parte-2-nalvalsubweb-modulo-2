Orientação a objetos - Atividade

Caso não tenha feito, faça o dever de casa da aula 01.
• Crie uma interface para representar a ideia de um imóvel que pode ser financiado (por ex. chamada Financiável).
    • A interface deve ter pelo menos um método abstrato, que represente a ação de financiar o imóvel.
• Crie uma nova classe que represente o objeto Item de Mostruário:
    • A classe deve ter um construtor que inicialize todos seus atributos
    • A classe deve ter getters e setters para todos os seus atributos
    • A classe deve ter, entre outros, os atributos: desconto, data de upload, data de venda.
• Faça a classe que você criou para representar o Apartamento no exercício anterior implementar a interface
Financiável (ela deve criar uma implementação qualquer para o método financiar).
• Ao mesmo tempo, a classe de Apartamento deve herdar da classe que representa o objeto Item de Mostruário e
fazer o override de um de seus métodos.
• Crie novas instâncias da classe de apartamento ou modifique as existentes para testar as modificações
realizadas.
